jQuery( document ).ready( function( $ ) {

    // Override default size via width attr
    var b_size = $('.container--ph').data('size');
    if (b_size != 'default') {
        //alert($('.container--ph').data('size'));
        $('.bf-container').css({'width':b_size});
    }
    // Override defualt color
    var b_color = $('.container--ph').data('color');
    if (b_color != 'default') {
        champstudio_bananaphone_change_color( $, b_color );
    }

    // Override the padding so the container matches the svg banana
    $('.container--ph').css({'padding-top':$('.svg--responsive').height()});

    // Change color on hover event
    $('.colorspot').mouseover(function() {
        var color = $( this ).css( 'background-color' );
        champstudio_bananaphone_change_color( color );
    });

    // Log banana phone height
    //console.log($('.svg--responsive').height());

    // Log length of banana phone main path
    /*    
    var path = document.querySelector('#Banana path');
    var length = path.getTotalLength();
    console.log(length);
    */

    /*
        Set the color of each section of the SVG
        and re-draw
    */
    function champstudio_bananaphone_change_color( $new_color = '#ffffff' ) {
        // Set stroke and fill colors
        $color = $new_color;
        $('#Banana').attr('stroke',$color);
        $('#Rectangle-1').attr('fill',$color);
        $('#Earpiece').attr('stroke',$color);
        $('#Antenna').attr('stroke',$color);
        $('#Oval-1').attr('fill',$color);
        $('#Line').attr('stroke',$color);
        $('#Polygon-1').attr('fill',$color);

        // Re-spawn object to reset animation
        $('.svg--responsive').replaceWith($('.svg--responsive').clone());
    }
});