<?php
/**
 * @package Champ Studio banana phone
 * @version 1.0
 */
/*
Plugin Name: Champ Studio banana phone
Description:  Display the Champ Studio banana phone logo in all its glory.
Author: Eric Marty
Version: 1.0
Author URI: http://www.champstudio.com
*/

/**
 * Create shortcode [bananaphone]
 */
function champstudio_bananaphone_func( $atts ){

    // Process colors
    $show_colors = FALSE;
    if (is_array($atts) && array_key_exists('colors',$atts)) {
        //error_log( print_r($atts, true) ); // /srv/logs
        if ($atts['colors'] == 'yes')
            $show_colors = TRUE;
    }
    // Process start color
    $start_color = 'default';
    if (is_array($atts) && array_key_exists('color',$atts)) {
        //error_log( print_r($atts, true) ); // /srv/logs
        $start_color = $atts['color'];
    }
    // Process size
    $size = 'default';
    if (is_array($atts) && array_key_exists('size',$atts)) {
        //error_log( print_r($atts, true) ); // /srv/logs
        $size = $atts['size'];
    }

    $wrap_start = '<div class="bf-container container--ph" data-color="' . $start_color . '" data-size="' . $size . '">';
    $wrap_end = '</div>';
    $graphic = file_get_contents(plugins_url( '/images/bananaphone.svg', __FILE__ ));
    $output = $wrap_start . $graphic . $wrap_end;

    // Display colors if option is selected
    if ($show_colors) {
        $colors = '<div class="container colorspots-container"><div class="colorspot spot-one"></div>';
        $colors = $colors . '<div class="colorspot spot-two"></div>';
        $colors = $colors . '<div class="colorspot spot-three"></div></div>';
        $output = $output . $colors;
    }

    return $output;
}
add_shortcode( 'bananaphone', 'champstudio_bananaphone_func' );

/**
 * Enqueue scripts and styles.
 */
function champstudio_bananaphone_scripts() {
    wp_enqueue_style( 'bananastyle', plugins_url( '/css/style.css', __FILE__ ) );
    wp_enqueue_script( 'bananascript', plugins_url( '/js/functions.js', __FILE__ ),  array( 'jquery' ), NULL, false );
}
add_action( 'wp_enqueue_scripts', 'champstudio_bananaphone_scripts' );

?>
